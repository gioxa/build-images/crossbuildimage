#!/usr/bin/bash
#
#  install_fake.sh
#
# Created by Danny Goossen on 28/12/2017.
#
# MIT License
#
# Copyright (c) 2018 odagrun, Gioxa Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
. trap_print
# Install the fake components into our image so the new image can be used to create new images.
#
if [ -z "$target" ]; then
  target=$PWD/rootfs
fi

if [ -d "/usr/sbin/glibc-fake" ]; then
  GLIBC_FAKE=/usr/sbin/glibc-fake
else
   echo -e "\033[35m /usr/sbin/glibc-fake NOT FOUND \n\n!!! use \'gioxa/imagebuilder\' docker image for build!!!\033[0;m\n"
   f_fail
fi
export GLIBC_FAKE

if [ ! -d "/${target}" ]; then
  echo -e "\033[35m target dir: \'$target\'\n\n!!! define \'\$target\', as an absolute path!!!\033[0;m\n"
  f_fail
fi
echo -e "\n\033[33m------- cp glibc ------\033[0;m\n"
mkdir -pv $target/usr/sbin/glibc-fake
cp $GLIBC_FAKE/* $target/usr/sbin/glibc-fake/
# ldconfig with FAKE_SKIP_LDCONFIG
if [ -x "/usr/sbin/realldconfig" ]; then
   mv -v $target/usr/sbin/ldconfig $target/usr/sbin/realldconfig
   cp -v /usr/sbin/ldconfig $target/usr/sbin/ldconfig
fi
cp -vf /usr/sbin/trap_print $target/usr/sbin/trap_print

  trap - DEBUG
