# ${ODAGRUN_IMAGE_REFNAME}


[![](https://images.microbadger.com/badges/version/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}.svg)](https://microbadger.com/images/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}) [![](https://images.microbadger.com/badges/image/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}.svg)](https://microbadger.com/images/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}) [![](https://images.microbadger.com/badges/license/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}.svg)](https://microbadger.com/images/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME} "Get your own license badge on microbadger.com") [![](https://img.shields.io/badge/Gitlab-source-blue.svg)](${CI_PROJECT_URL})

## Purpose

Create a micro Centos 7 docker image, in a single layer to build gcc/clang cross-compile toolchains.


## yum config

```bash
# YUM:

${OS_CONFIG_CONTENT}

```

## Image Config

```yaml
$DOCKER_CONFIG_YML
```

## usage

For use with gitlab-ci

```yaml
image: ${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}:latest
```


*Build with [odagrun](https://www.odagrun.com) on openshift-online-starter*
