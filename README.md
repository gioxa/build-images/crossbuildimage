# cross compiler build image

[![build status](https://gitlab.com/gioxa/build-images/crossbuildimage/badges/master/build.svg)](https://gitlab.com/gioxa/build-images/crossbuildimage/commits/master)

## Purpose

Create a micro Centos 7 docker image, in a single layer to build gcc/clang cross-compile toolchains.

## Build contents:

from `make_os.conf`:

```
install_packages="zlib-devel yum-utils bzip2 coreutils tar xz /usr/local/src/FAKE_RPMS/*.rpm mpfr* gmp* libmpc"
install_groups="Development Tools"
```

## Extras needed to make a rootfs from rpm repository

1. fakechroot
2. fakeroot
3. fake_arch
4. fake_LDCONFIG

## Concept Usage

1. use combination of fakechroot,fakeroot and fake_arch to create `/usr/lib` and `/usr/include` containing the target libraries.
2. `mv /usr/lib` and `/usr/include`  to `/opt/gcc-$TARGET/$TARGET` with e.g. `TARGET=i386-el6-linux-gnu`
3. build binutils with `TARGET` from srpm `centos el7`
4. build gcc with `TARGET` from srpm `centos el7`
5. wrapp `/opt/*` and add it to a host system!
